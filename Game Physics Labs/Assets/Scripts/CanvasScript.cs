﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasScript : MonoBehaviour
{
    string eulerString = "Update Current Method: Euler";
    string kinematicString = "Update Current Method: Kinematic";
    Text textBox = null;
    Button swapButton = null;
    Particle2D pScript = null;
    void Start()
    {
        textBox = gameObject.transform.GetChild(0).GetComponent<Text>();
        swapButton = gameObject.transform.GetChild(1).GetComponent<Button>();
        swapButton.onClick.AddListener(ToggleUpdateMethod);
        pScript = GameObject.FindGameObjectWithTag("Cube").GetComponent<Particle2D>();
    }

    
    void Update()
    {
        
    }

    public void ToggleUpdateMethod()
    {
        pScript.ToggleUpdateMethod();
        
        if(pScript.currentUpdateMethod == Particle2D.UpdateMethod.Explicit)
        {
            textBox.text = eulerString;
        }
        else
        {
            textBox.text = kinematicString;
        }
    }

}
