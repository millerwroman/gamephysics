﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle2D : MonoBehaviour
{
    //Step 1
    public Vector2 position, velocity, acceleration;
    public float rotation, angularVelocity, angularAcceleration;

    public enum UpdateMethod {Explicit, Kinematic};
    public UpdateMethod currentUpdateMethod = UpdateMethod.Explicit;

    //Step 2
    void UpdatePositionEulerExplicit(float dt)
    {
        //Position
        position += velocity * dt;

        //Velocity
        velocity += acceleration * dt;
    }

    void UpdatePositionKinematic(float dt)
    {
        //Position
        position += velocity * dt + 0.5f * acceleration * dt * dt;

        //Velocity
        velocity += acceleration * dt;
    }

    void UpdateRotationEulerExplicit(float dt)
    {
        rotation += angularVelocity * dt;

        angularVelocity += angularAcceleration * dt;

        rotation = rotation % 360;
    }

    void UpdateRotationKinematic(float dt)
    {
        rotation += angularVelocity * dt + 0.5f * angularAcceleration * dt * dt;

        rotation += angularAcceleration * dt;

        rotation = rotation % 360;
    }

    public void ToggleUpdateMethod()
    {
        if(currentUpdateMethod == UpdateMethod.Explicit)
        {
            currentUpdateMethod = UpdateMethod.Kinematic;
        }
        else
        {
            currentUpdateMethod = UpdateMethod.Explicit;
        }
    }

    void Start()
    {
        
    }

    
    void FixedUpdate()
    {
        //Step 3
        //UpdatePositionEulerExplicit(Time.fixedDeltaTime);
        switch(currentUpdateMethod)
        {
            case UpdateMethod.Explicit:
                UpdatePositionEulerExplicit(Time.deltaTime);
                UpdateRotationEulerExplicit(Time.deltaTime);
                break;
            case UpdateMethod.Kinematic:
                UpdatePositionKinematic(Time.deltaTime);
                UpdateRotationKinematic(Time.deltaTime);
                break;

        }

        transform.position = position;
        transform.rotation = Quaternion.Euler(0, 0, rotation);

        //Step 4
        acceleration.x = -Mathf.Sin(Time.fixedTime);
        acceleration.y = -Mathf.Cos(Time.fixedTime);
        angularAcceleration = -Mathf.Sin(Time.fixedTime);
    }


}
